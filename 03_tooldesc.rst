The Tool Box
============

The descriptions of the tools has been arranged to be grouped under the stages
of the pipeline they address. This can be useful in getting a context for the
problem that the tool addresses during production.


.. toctree::
   :maxdepth: 2

   toolgroups/productionplanning
   toolgroups/assetsproduction
   toolgroups/layoutanimation
   toolgroups/lightingrendering
   toolgroups/post
