const gulp = require('gulp');
//const exec = require('gulp-exec');
const exec = require('child_process').execSync;
const bs = require('browser-sync');

const files = {
    rst: '*.rst',
    images: 'images/*.png',
    tools_rst: 'tools/*.rst',
    toolgroups_rst: 'toolgroups/*.rst',
    graphviz: 'graphs/*.gv',
}

gulp.task('makehtml', function (cb) {
    exec('make html');
    cb();
});

gulp.task('makelatexpdf', function(cb) {
    exec('make latexpdf');
    cb();
});

function watchTask() {
    console.log('Now watching for changes ...');
    gulp.watch(
        [files.rst,
            files.tools_rst,
            files.toolgroups_rst,
            files.images,
            files.graphviz],
        {interval: 1000, usePolling: true},
        gulp.series(
            gulp.parallel('makehtml'),
            watchTask)
    );
}


exports.default = gulp.series(watchTask)
