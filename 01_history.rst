History
-------

Legacy of ICE Animations
~~~~~~~~~~~~~~~~~~~~~~~~

ICE Animations has been an important contributor to the local animations and
VFX industry in Pakistan in a variety of ways. It is one of the few
institutions that has actively spent its time and resources in experimenting
with growth models.

For example, it has held the reputation of being the place where young
aspirants in CG get professional training and their first exposure with local
and international clients. Many have moved on to creating their own businesses
and on to jobs in international studios.

Similarly, ICE Animations was also the only local studio to train and support
a number computer engineers working on various aspects of CG. The pipeline
team was also a part of such experiments laid out to improve quality and
velocity of production.


Influential Projects
~~~~~~~~~~~~~~~~~~~~

On the outset the financial and professional motivation of working in CG is
towards the movie and gaming industry in the west, and there have been some
VFX features executed locally that address that motivation. But, technically
some of the projects that originated closer have been more Influential.

`Bankay Mian` was a TV show of temporary local fame because it addressed
politics via a comical cartoon character, however, there were some technical
achievements behind it.  Firstly, it involved use of a state of the art motion
capture system at that time. However, its pipeline was an important piece of
the puzzle. The repetitive and limited scope of the production meant that
automation could deliver many minutes of quality content per day with limited
computing and human resources.

`Mansour` was a emirati TV show which required use of same assets over many
episodes and several seasons. As more episodic animations project were
acquired, the need for a production pipeline emerged to solve the recurring
issues. This was a classic opportunity for developing a pipeline and remained
the main motivation behind building of a new team.


The Pipeline Team
~~~~~~~~~~~~~~~~~

Back in 2012, A new team of raw computer programmers was given the mandate for
getting trained in CG software and aimed to address production issues using
`agile` development practices.

The pipeline team started with delivering tools for workflow and automation,
addressing specific intra departmental issues. Later the effort focused on
creating systems and addressing the pipeline as a whole by arranging tool
menus and developing a digital asset management system.

The nature of software projects is such that they are perpetually under
development. It requires experimentation and working through failures. So, it
is during the course of the last three seasons of the `Mansour` and other
similar projects a pipeline evolved which had a noticeable positive impact in
the effective utilization of resources and helping artists focus on
creativity.

The Way Forward
~~~~~~~~~~~~~~~

The local industry in Pakistan has now moved to newer model where the core
teams are small and the type and size of projects has either remained
unchanged or increased, and there is more reliance on project-based contracts and
freelance artists. 

While, in the current scenario the need for a permanent team to look after the
pipeline has increased, the studios are unable to afford such a team on their
own. 

One solution could be the model of the electric company where one dedicated
team can be used to address the issues faced by many different studios and
production teams, and build on the work done over the years. The Nebula
Pipeline initiative is an effort that hopes to address this gap of needs with
this newer business model.
