.. _scene-bundle:


Scene Bundle
============

:ref:`scene-bundle` is an :ref:`rendering <lighting-rendering>` tool that runs inside of
:ref:`Maya <software-maya>` and as well as a standalone application that helps
rendering artists to bundle their scenes by copying all its requirement into a
single packet and sending them as source for rendering jobs on the
:ref:`deadline <software-deadline>` cluster.


Screen Shot
-----------

.. figure:: ../images/scene-bundle.png

   Remap Textures running inside maya.

Features
--------
* Collect all dependencies from a rendering scene to a single `maya` project
  folder called a `bundle`.
* Create an archive from the `bundle`
* Copy the `bundle` to an appropriate location ready for rendering.
* Send a job to the :ref:`deadline <software-deadline>` according to
  predefined parameters.


Usage
-----
1. Provide a name for the bundle.
#. Provide a location for the bundle most suitable for collection. This
   location should have ample disk space to collect all the dependencies.
#. If you intend to bundle the current scene check ``current scene`` check
   else provide a list of scenes to process.
#. If submitting to deadline provide the `project`, `episode`, `sequence`, and
   `shot` information either by drop down or by clicking on the list and using
   the UI that appears.
#. Hit ``Create`` to start the bundling process.
#. Attend to ignore or raise the appearing error prompts if any.


Requirements
------------
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Thinkbox Deadline <software-deadline>`
