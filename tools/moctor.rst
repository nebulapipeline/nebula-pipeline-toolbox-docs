.. _moctor:

Moctor
======

:ref:`moctor` or `MOCap TO Rig` is an :ref:`Animation <layout-animation>`
tools useful in `retargetting` motion capture or skeleton data from other
sources onto a rig such as
`Advanced Skeleton <https://www.animationstudios.com.au/advanced-skeleton>`_

A popular use case is to use `Mixamo <https://mixamo.com>`_ downloads to map
onto `advanced skeleton <https://www.animationstudios.com.au/advanced-skeleton>`.
This functionality can be extended to other input and output hierarchies.

Screen Shot
-----------

.. figure:: ../images/moctor.png

   Moctor (MEL-based) UI running inside of maya

Features
--------

* Prepping Rig for retargetting
* Importing Motion Capture (skeleton-based) animation data into maya.
* Retargetting imported animation data on to a `rig` using Maya's `HumanIK`
  technology.
* Baking animation data on the rig controls.
* Clean up rig scene after animation import.


Usage
-----
* Specify the mocap mapping name from drop down menu
* Provide Path for the `fbx` file.
* hit ``Import Mocap`` to import the fbx file.
* hit ``Goto T-pose Frame`` to go to frame `0`
* If the mocap data does not contain the T-pose frame on frame `0`. You should
  create one by hitting ``Mocap Zero Out``, ``Fix Mocap T-pose``, ``Set Key``
* Specify the `Rig` mapping.
* Provide the path for `rig`.
* hit ``Import Rig`` to `reference` the scene file onto the current scene.
* If the T-pose is not proper hit ``Fix Tpose`` button on the `rig` side to fix
  it.
* Now we have both mocap and Rig in T-pose. Now hit ``Map Both to HIK`` to
  perform retargetting onto the `rig`. The rig should now have animation
  mapped onto it.
* Hit ``Bake Animation`` and ``Cleanup Both HIK`` button to bake and cleanup
  the scene.
* Now you use ``Delete Mocap`` to remove mocap data from scene.
* If required use the ``Import Rig Reference`` button to import the rig and
  remove namespace.
* ``Select Rig Controls`` button can be useful when exporting animation data
  for posing tools such as `studio library <https://www.studiolibrary.com>`_


Requirements
------------
* :ref:`Autodesk Maya <software-maya>`
