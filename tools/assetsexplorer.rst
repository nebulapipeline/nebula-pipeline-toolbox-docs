.. _assets-explorer:

Assets Explorer
---------------

Asset Explorer is a comprehensive :ref:`assets-production` tool that can be used
to Create, Read, Update and Publish Assets.

The Asset Explorer is intended to run inside
of :ref:`Autodesk Maya <software-maya>`
but it can also run on :any:`Microsoft Windows <software-windows>` as a
standalone application.

Screen Shot
~~~~~~~~~~~

.. figure:: ../images/asset-explorer.png

   The Asset Explorer UI running inside of maya


Features
~~~~~~~~

* List all assets, contexts and versions related to a project.
* Import, Reference or CheckOut any asset version along with dependencies.
* Save assets to the :ref:`Tactic <software-tactic>` server along with
  texture.
* Pair assets which quality for cache compatibility.
* Publish assets to `episodes` etc. for use in production.

Usage
~~~~~

The typical workflow of the asset explorer can be as follows:

* The user selects a project from the listing to get a list of assets from the
  tactic server.
* Filter the assets using the search box on the top of the left most column
* Select the context of the asset you need to work with such as model, rig or
  shaded to show the various versions published to the context.
* Select a version and hit the `import` or `reference` button to import or
  reference the required version.
* While a context is selected you can use the `save` button peform
  :ref:`asset-saving`.
* Right Clicking on a version will reveal the context menu which can be used
  to either perform :ref:`asset-pairing` or :ref:`asset-publishing`


.. _asset-saving:

Asset Saving
''''''''''''

When the save button is pressed this reveals the Asset Check In dialog which
can be used to create a new asset version in the selected context from the
current maya scene.

.. figure:: ../images/checkinput.png

   The Check In dialog that allows you to save the current maya scene file to
   :ref:`Tactic <software-tactic>`

Optionally the user can select geometry from the open scene to export as a
:ref:`Redshift <software-redshift>` proxy or a GPU cache (alembic) file.

The following behaviours are also associated with file saving:

* While saving a file it must contain a `geo_set` which is a maya set
  containing geometry nodes which must have a suffix of `geoset` in its name.
  The absence of such a set or a presence of an empty set renders the scene
  invalid and the asset explorer will show an error dialog.

* While saving files in the `shaded` context all the textures referenced in
  the current scene are also uploaded to the tactic server in the assets
  `texture` with their paths being updated to reflect their new location on
  the server.

.. _asset-pairing:

Asset Pairing
'''''''''''''

Asset Pairing is the process of finding out if a particular version of asset
rig file is cache compatible with a certain version of asset model or shaded
file.

Once an asset pairing is performed this information is stored into the tactic
database and the asset explorer can then be used to publish the asset to
an episode in production.

.. note::
    Asset pairing is not required and unavailable for `environment` and
    `vegetation` :ref:`asset categories <asset-categories>`

.. _asset-publishing:

Asset Publishing
''''''''''''''''

.. figure:: ../images/publish.png

   A publishing interface showing if the current asset is publishable in the
   selected episode

Asset Publishing is the process of saving a version of an asset for use in a
particular element of production most typically an `episode`.

.. note::
    When an environment is published all the GPU and proxy
    references containing are also published along with it.

When an asset is published all the files related to that versions are copied
to the directory of the particular episode and a versionless current version
is made available for use in production.

.. note::
    When a shaded model is published all its referenced textures are published
    along with it

Requirements
~~~~~~~~~~~~
* :ref:`Windows OS <software-windows>`
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Southpaw Tactic <software-tactic>`

