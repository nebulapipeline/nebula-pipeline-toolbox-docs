.. _multi-shot-export:

Multishot Export
================

:ref:`multi-shot-export` is a :ref:`layout-animation` tool that runs inside of
:ref:`Maya <software-maya>` that helps in exporting animation info from
`sequence` level scenes or scenes that contain multiple shots. It works best
when it is used on scene which have been created using the
:ref:`create-layout` tool.


.. tip::

    :ref:`multi-shot-export` works best when dealing with scenes created by
    the :ref:`create-layout` tool


Screen Shot
-----------

.. figure:: ../images/multishot-export.png

   Multishot Export tools running inside maya.


Features
--------

.. info::
   One older version of multishot export works independently of the `tactic`
   server, and has been found useful in working off-site such as in case of
   freelance artists

* List the shots present inside the current animation scene.
* Switch from one shot to another 
* Export the following from all or multiple shots in your scene
  + `Point Cache` for each or selected `character`, `prop` and `vehicle`
  + `Preview` for each camera in the scene.
  + :ref:`Maya <software-maya>` and :ref:`Nuke <software-Nuke>` format camera
    animation.
* Burn overlays onto the preview showing useful information about the shot.


.. figure:: ../images/multishot-export-output.png

   Output of the multishot export showing info using `Overlays`

* Export the output directly to :ref:`tactic <software-tactic>` server or
  export to a local directory.
* Defer the extraction to a `multshot` :ref:`Deadline <software-deadline>`
  pool to save time for artists.

.. tip::
   Save your machine time by offloading preview, cache and cam extraction
   tasks to deadline


Usage
-----

* Fire-up the multishot export in an `animation` or `layout` scene, which has
  preferrably been created using the :ref:`create-layout` tool. Multishot will
  show the `project`, `episode` and `sequence` information stored in the
  scene.
* Select the shots you want to work with from the drop down list at the
  bottom.
* For each shot select the `rigs` whose cache you wish to export.
* For each shot select the `display layers` you want to make appear in the
  preview video.
* For each shot you can choose if you want to bake and export the camera.
* To perform extraction do:

    - Either Hit `Export` to begin extraction immediately
    - Or Hit `submit` to defer the extraction procedure to the `multishot`
      :ref:`deadline <software-deadline>` tool.


Requirements
------------
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Southpaw Tactic <software-tactic>`
* :ref:`FFMPEG <software-ffmpeg>`
