.. _episode-planner:

Episode Planner
~~~~~~~~~~~~~~~

Episode Planner is a :ref:`production-planning` tool used for population of episodes from a folder
whose hierarchy describes the various shots and sequences of an episode.

Features
""""""""

* Populate the tactic database with sequence, shots and frame ranges from
scanning the filesystem

Screen Shot
"""""""""""
.. figure:: ../images/episode-planner.png

   Episode Planner Screen Shot

Usage
"""""

.. note::
   This tool assumes a specific hierarchy which can be achieved from use of
   templates in conforming software such as `Hiero` or `Nuke studio`.

* Cut the animatic into shots and sequences, and perform an export operation.
* Specify the `project` and `episode` from the drop-down list.`
* Provides the output path from the conforming software
* Hit `Populate`


.. warning::
   Do not perform this operation after the episode has gone into production.
   This may make you lose data related to production.

.. tip::
   Use this tool only when the episode is being initiated.


Requirements:
"""""""""""""
* :ref:`Windows OS <software-windows>`
* :ref:`Southpaw Tactic <software-tactic>`
