.. _add-assets:

Add Assets
----------

:ref:`add-assets` is a :ref:`lighting-rendering` intended to be used at the time of lighting
scene assembly. This tool runs inside of :ref:`Maya <software-maya>` in order to
import assets into the current scene.

Screen Shot
~~~~~~~~~~~

.. figure:: ../images/add-assets.png

   Add Assets UI running inside of maya

Features
~~~~~~~~

* List publised assets for one of more sequences according to the specified
  context out of `rig`, `model`, `shaded` and `shaded/combined`
* Select and Reference all the required assets with multiplicity

Usage
~~~~~

The add assets tool fetches the projects and sequence from the and when the user selects a sequence to work with, all the
assets available for that sequence are listed. 

* Select a project from the drop down, the episodes drop down will be
  populated from the :ref:`Tactic <software-tactic>` server.
* Select an Episode from the drop down to populate the sequences.
* Select one of more sequences to list all assets of the given context.
* Change the context from the drop down on the bottom to fetch the list of
  relevant files.
* Select the required assets, specifying the number of instances in front of
  it.
* Hit the `reference` button to reference all the specified assets into the
  scene in one go.

Requirements
~~~~~~~~~~~~
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Southpaw Tactic <software-tactic>`


