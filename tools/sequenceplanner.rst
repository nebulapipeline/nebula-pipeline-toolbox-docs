.. _sequence-planner:


Sequence Planner
=================

:ref:`sequence-planner` is an :ref:`production-planning` tool that runs as a
standalone application that helps to associate assets to sequences. This is a
necessary step to perform before creating layouts using the :ref:`create-layout`
tool.


Screen Shot
-----------

.. figure:: ../images/sequence-planner.png

   Sequence Planner UI


Features
--------

* List all the `assets` and `sequences` related to an episode.
* Add the required assets under the relevant sequences


Usage
-----

1. Select `project` and `episode` from the drop down list on top to populate
   `assets` and `sequences` the top and bottom boxes.
2. Select a number of assets from the list above and click the ``+`` button on
   the relevant sequence to add the assets to the sequence. The changes are
   stored in the database immediately.


Requirements
------------
* :ref:`Southpaw Tactic <software-tactic>`
* :ref:`Windows <software-windows>`
