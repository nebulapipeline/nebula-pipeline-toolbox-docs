.. _pre-cc:

Pre-CC
------

Pre-CC is a tool used by rendering artists to automatically composite the
render output to allow to check visually for errors and missing frames.

.. note::

   This program invokes `Nuke` in the background to perform the composite.


Screen Shot
~~~~~~~~~~~

.. figure:: ../images/pre-cc.png

   Pre-CC Screen Shot


Features:
~~~~~~~~~

* Generate composited outputs from renders from sequences by scanning the file
  system.
* Add overlays on the output describing the `shot` and `frame #`
* Ability to control composite method using underlying nuke scene

.. figure:: ../images/pre-cc-output.png

   Output of :ref:`Pre CC <pre-cc>` played in QuickTime with overlays


Usage
~~~~~

.. figure:: ../images/precc_creating_comps.png

   Pre-CC while creating comps

* Provide the path for a sequence for render location.
* Select shots that need to be checked
* hit `Start`.


Requirements
~~~~~~~~~~~~
* :ref:`Windows <software-windows>`
* :ref:`Nuke <software-nuke>`
