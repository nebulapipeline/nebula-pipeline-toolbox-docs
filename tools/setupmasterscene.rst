.. _setup-master-scene:


Setup Master Scene
==================

:ref:`setup-master-scene` is an :ref:`production-planning` tool that runs as a
standalone application that helps to associate assets to sequences. This is a
necessary step to perform before creating layouts using the :ref:`create-layout`
tool.


Screen Shot
-----------

.. figure:: ../images/setup-master-scene.png

   Setup Master Scene UI from Inside of :ref:`Maya <software-maya>`


Features
--------

* Prep the Environment in the scene for Rendering

  + Setup `Beauty` and `Occlusion` render layers with proper material
    overrides.
  + Add relevant `lights` and `meshes` to the scene
  + Setup `parameter sets` for the environment such as `Env_Smooth_Set`,
    `Env_Matte_Set` and `Env_Vis_Set`

* Setup Characters for Rendering

  + Setup `Beauty` and `Shadow` render layers.
  + Add character Lights to the render layers
  + Add passes such as `Puzzle Matte` object and material Ids.
  + Setup environment parameter sets for interaction with environments
  + Setup `parameter sets` for the characters such as `Char_Smooth_Set`,
    `Char_Matte_Set` and `Char_Vis_Set`


Usage
-----

1. Add all the required assets to a maya scene using the :ref:`add-assets`
   tool which can also be invoked from the ``Sequence Assets`` button.
#. Create a group called ``characters`` and add all the mesh node of the
   `characters`, `props` and `vehicles` to this group
#. Create a group called ``environment`` and add all the mesh node from the
   environment to this group.
#. Create a group called ``env_lights`` and add all the environment lights.
#. Create a group called ``char_lights`` and add all the character lights.
#. Choose the resolution from the drop down list and Check all the options you
   want in the setup!
#. Hit the ``Start`` button to setup the master scene.
#. If so required, add an optional `fresnel` render layer by hitting
   ``Fresnel`` button.


Requirements
------------
* :ref:`Southpaw Tactic <software-tactic>`
* :ref:`Autodesk Maya <software-maya>`
