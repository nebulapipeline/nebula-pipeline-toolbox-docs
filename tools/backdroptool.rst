.. _backdrop-tool:

Backdrop Tool
-------------

The :ref:`backdrop-tool` is a set of compositing scripts that help automate
the process of compositing from inside :ref:`Nuke <software-nuke>`.


Screen Shot
~~~~~~~~~~~

.. figure:: ../images/backdrop-tool.png

   The Backdrop tool UI inside of :ref:`Nuke <software-nuke>`

Features
~~~~~~~~

The features have been presented below under :ref:`Backdrop Tools` and
:ref:`Read Node Tools` according to the context of their actions.

.. tip::
   Arrange all the nodes from one shot into a single ``Backdrop`` node in
   order to gain advantage of all the :ref:`Backdrop Tools`

Backdrop Tools
==============

Replace Read Path
   Given a backdrop containing the network of nodes that can
   render one shot perfectly, The `Replace Read Path` feature can either replace
   all the relevant knobs of the read and write nodes with the render output
   related to another shot or replicate this layout for other shots by scanning
   the render output on the provided path.

Add Write Node
   Given a node under selection one can add a write node after it and replace
   its knobs with the appropriate ranges and output paths.

Replace Cameras in Backdrop
   Given a backdrop containing a camera node, the node is replaced by
   appropriate camera for the given shot.

Replace Render Layer
   Detects all the render layers being used in the current selection / backdrop and
   replace the render layers with the other ones that are available in the
   file system containing the render outputs

Read Node Tools
===============

Select Error Nodes
   Select all the Read nodes which have error.

.. tip::
   Use the `Select Error Nodes` feature to select all the nodes that have
   error and then use them in tandem with the rest of the tools to change them
   all at once.

ReRead Frame Range
   Update frame range on the selected read nodes by scanning the filesystem.

Set Nearest Frame
   Change the on error behaviour of the selected nodes to nearest frame.

Auto Increment Save
   Automatically save the current nuke script after the given interval.

Red To Default
   Change the color of the `red` or error nodes to default.

.. hint::
   All the tools present under `Read Node Tools`_ work selected read nodes in
   nuke.

Third Party Tools
=================

The following 3rd Party Tools can also be invoked from this UI for easy
access.

RenderThreads
   This is a popular rendering scripts available on 
   `Nukepedia <https://www.nukepedia.com/python/render/renderthreads>`_ which
   can be used to perform renders asynchronously


Requirements
~~~~~~~~~~~

* :ref:`The Foundry Nuke <software-nuke>`
