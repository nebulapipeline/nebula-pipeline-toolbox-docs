.. _shader-transfer:


Shader Transfer
===============

:ref:`shader-transfer` is an :ref:`assets-production` tool that runs inside of
:ref:`Maya <software-maya>` which helps in transferring shaders from one maya
object to another.


Screen Shot
-----------

.. figure:: ../images/shader-transfer.png

   Shader Transfer UI from Inside of :ref:`Maya <software-maya>`


Features
--------

* Transfer shaders and UV sets from single mesh to multiple single mesh
* Transfer shaders and UV sets from members of a set to members of multiple
  sets.


Usage
-----

1. Select `Transfer Policy` from the drop down list either
   ``Single to Single`` or ``Set to Set``.
#. Select the `source` mesh or set and hit ``Add Source`` button.
#. Select the `target` meshes or sets and hit ``Add Targets`` button.
#. Check the ``Transfer UVs`` button if required.
#. Hit ``Transfer`` to start the process.


.. note::

   The number of faces on the source and target meshes should be the same if
   the shaders are assigned to individual faces.


Requirements
------------
* :ref:`Autodesk Maya <software-maya>`
