.. _proxy-cache-switch:


Proxy Cache Switch
==================

:ref:`proxy-cache-switch` is a generic production tool that runs inside of
:ref:`Maya <software-maya>` that helps users in switching from `proxies` and
`gpu caches` or changing them from `hi-res` to `low-res`.


Screen Shot
-----------

.. figure:: ../images/proxy-cache-switch.png

   Proxy Cache Switch running inside maya.


Features
--------
* Switch between `proxy` and `gpu-cache` versions of the same object.
* Switch between `hi-res` and `low-res` versions of the same object.
* Change properties on `proxy` and `cache` nodes.
* Export proxies and paths from the scene.

Usage
-----
* In order to switch from `gpu-cache` from `proxy`, hit the ``G`` button in
  front of it.
* To switch from `proxy` to `gpu-cache` hit the ``P`` button.
* To switch between `hi-res` and `low-res`, hit the ``H/L`` button.

Requirements
------------
* :ref:`Autodesk Maya <software-maya>`
