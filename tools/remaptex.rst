.. _remap-tex:


Remap Textures
==============

:ref:`remap-tex` is an :ref:`assets-production` tool that runs inside of
:ref:`Maya <software-maya>` that helps artists to adjust paths of texture
nodes after they have been relocated to a new path.


Screen Shot
-----------

.. figure:: ../images/remap-tex.png

   Remap Textures running inside Maya.

Features
--------
* List all the paths in the scene referring to texture nodes.
* List all the paths in the scene referring to `redshift` proxy nodes.
* Highlight the missing texture from the paths
* Change the path of all the textures to indicate change of location.

Usage
-----

1. Make sure to hit `Refresh` to show status of the current scene.
#. The missing texture paths are highlighted.
#. Provide the new folder locations for the path you want to modify.
#. Hit ``remap`` to chane the paths on the nodes. 


Requirements
------------
* :ref:`Redshift 3D <software-redshift>`
* :ref:`Autodesk Maya <software-maya>`
