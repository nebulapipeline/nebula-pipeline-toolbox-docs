.. _create-shots:

Create Shots
-------------

:ref:`create-shots` is a :ref:`Lighting <lighting-rendering>` tool intended to
help lighting artists in creating the scene file for each `shot` after a
master file for the `sequence` has been created.


Screen Shot:
~~~~~~~~~~~~

.. figure:: ../images/create-shots.png

   The Create Shots tools running inside :ref:`Maya <software-maya>`


Features:
~~~~~~~~~

* Generate multiple lighting scene assembly for shots from a master shot.
* Automatically find and replace available caches.
* Create collage using the output of stills from the shots in order to check the files.

.. figure:: ../images/create-shots-mapping.png

   UI for modifying the mapping for create shots

* Modify mapping between objects and caches found on storage.

Usage:
~~~~~~

1. Create a `sequence` "Master Scene" using :ref:`add-assets` and
   :ref:`setup-master-scene` scripts.
#. Provide a shots dir to scan and populate shots in the drop down.
#. Select the shot(s) for which the scenes are to generated.
#. Check on `Create Files` and `Create Collage` as required.
#. Hit `Start`.
#. When the mapping UI shows up adjust the mappings and select the appropriate
   name for the layers from the list. Press ``OK`` to proceed.


Requirements:
~~~~~~~~~~~~~
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Window OS <software-windows>`
* :ref:`Image Magick <software-image-magick>`
