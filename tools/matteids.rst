.. _matte-ids:

Matte IDs
=========

:ref:`matte-ids` is a tool for :ref:`lighting-rendering` artists that runs in
:ref:`Maya <software-maya>`. It is useful in helping organizing object and
material ids into AOV channels.

Screen Shot
-----------

.. figure:: ../images/matte-ids.png

   The Matte IDs UI running inside of :ref:`Maya <software-maya>`

Features
--------

* Create New `Matte AOVs` of `material` or `object` type.
* List Unassigned materials and objects.
* Assign Material to each channel of the id.


Usage
-----------

* Switch Mode to `Material IDs` or `Object IDs` by hitting the button at the
  bottom.
* Hit `Refresh` to List all the assigned and unassigned materials / objects
* Select Unassigned materials from the list on the bottom and hit
  `Add Selection` to assign redshift AOV channels to the ids
* `Drag` and `Drop` in the table to shift around IDs in AOVs.


Requirements
-----------
* :ref:`Redshift 3D <software-redshift>`
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Southpaw Tactic <software-tactic>`
