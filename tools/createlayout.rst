.. _create-layout:

Create Layout
-------------

:ref:`Create Layout <create-layout>` is an :ref:`Animation <layout-animation>`
tool used for initiating animations the sequence level.

Screen Shot
~~~~~~~~~~~

.. figure:: ../images/create-layout.png

   The Create Layout GUI docked inside :ref:`Maya <software-maya>`

Features
~~~~~~~~

The most prominent features of the `Create Layout`_ tool are as follows:

* Create the layout file for the sequence by `referencing` all the required assets
  from the published stream.
* Save the layout :ref:`Autodesk Maya <software-maya>` file to the sequence
  in the tactic server.
* Conform the frame ranges for the scene cameras to sync with those stored in
  the :ref:`Tactic <software-tactic>` database.
* Export stills from each shot in layout as preview.

.. hint::
   The `Create Layout`_ tool lists and references all the assets published to
   the episode of selected layout.

Usage
~~~~~

The typical workflow of the create layout tool can be as follows:

* Select the `project`, `episode` and `sequence` from the drop down list to list all
  the assets published to the selected `episode`.
* Select all the shots for which the layout needs to be created.
* Select and add all the required assets to each shot where they feature.
* Hit the `Create` button to import all the required assets including the
  environment.
* Place the camera and assets on the required location in the environment by
  moving and keying them in place. Pose the characters, if required.
* Hit the `Save` button and specify the suffix for the name of the layout.

.. tip::
   Selecting all the shots from the drop down list is a standard practice when
   creating a single layout file for the whole sequence. It works generally
   because the assets throughout the sequence remain the same.

.. tip::
   Often more than one layout files are created if the environment through the
   sequence changes. e.g. Interior and Exterior.

Requirements:
~~~~~~~~~~~~~
* :ref:`Autodesk Maya <software-maya>`
* :ref:`Southpaw Tactic <software-tactic>`


