.. _redshift-aov-tools:

Redshift AOV Tools
==================

:ref:`redshift-aov-tools` is a :ref:`lighting-rendering` collection of some
simple but useful **important** automations which help you in building
:ref:`redshift <software-redshift>` rendering scenes. This tool also helps in
setting up rendering standards that are of great use in :ref:`post-production`


Screen Shot
-----------

.. figure:: ../images/redshit-aov-tools.png

   Redshift AOV Toolbar in the middle

Features
--------

* Add all the required passes to the scene as required
* Add Matte ID AOVs such as `Material IDs` and `Object IDs`.
* Set the AOV and filename prefixes in order to have consistency in render
  output naming.


Usage
-----

* Click on the buttons to perform the changes in a :ref:`maya <software-maya>`
  scene.

Requirements
------------
* :ref:`Autodesk Maya <software-maya>`
