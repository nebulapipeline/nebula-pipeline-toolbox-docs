The Last Word
=============

This guide is just an introductory documentation of the tools at hand. There
is need for more literature in terms of more user and developer documentation
which is required to make a complete product out of this effort.

As more well rounded efforts are done to standardize the underlying library
and more back-ends are integrated, the need for comprehensive documentation is
also mandatory.

Please send any questions and corrections to `Talha Ahmed
<talha.ahmed@gmail.com>`_
