#!/bin/sh

echo stopping gulp ...
pkill -9 gulp

echo stopping browser-sync ...
ps aux | grep node.*browser-sync | grep -v grep | awk {'print$2'} | xargs kill -9
