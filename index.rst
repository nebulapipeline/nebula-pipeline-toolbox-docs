.. Nebula Pipeline Toolbox documentation master file, created by
   sphinx-quickstart on Fri Mar 13 19:03:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nebula Pipeline Toolbox
=======================

.. toctree::
   :maxdepth: 3

   Introduction <00_intro>
   History <01_history>
   Background <02_background>
   The Nebula Pipeline Tool Box <03_tooldesc>
   The Last Word <04_conclusion>

..  Indices and tables
    ==================

    + :ref:`genindex`
    + :ref:`modindex`
    + :ref:`search`
