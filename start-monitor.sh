#!/bin/sh

echo running gulp ...
gulp &

echo running browser-sync ...
pushd _build/html
browser-sync start --server --files *.html &
popd
