.. _production-planning:

Production Planning
-------------------

.. figure:: ../images/phase-planning.png

   Planning Phase


.. toctree::
   :maxdepth: 1

   ../tools/episodeplanner
   ../tools/sequenceplanner
