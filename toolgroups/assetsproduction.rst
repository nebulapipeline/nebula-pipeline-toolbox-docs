.. _assets-production:

Assets Development
------------------

.. figure:: ../images/phase-assets.png

   Assets Phase

.. toctree::
   :maxdepth: 1

   ../tools/assetsexplorer
   ../tools/remaptex
   ../tools/shadertransfer
   ../tools/proxycacheswitch


.. _asset-categories:

.. rubric:: Asset Categories
Assets can be classified into the following `Asset Categories`:

1. Characters
2. Environments
3. Props
4. Vehicles
5. Vegetation

From the above asset categories only Characters, Props and Vehicles require a
`rig` for animations while Environment and Vegetation only require `model` and
`shaded` contexts.
