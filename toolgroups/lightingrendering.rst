.. _lighting-rendering:

Lighting and Rendering Tools
============================

.. figure:: ../images/phase-images.png

   The Lighting and Rendering Phases

.. toctree::
   :maxdepth: 1

   ../tools/addassets
   ../tools/setupmasterscene
   ../tools/createshots
   ../tools/matteids
   ../tools/redshiftaovtools
   ../tools/precc
   ../tools/scenebundle
