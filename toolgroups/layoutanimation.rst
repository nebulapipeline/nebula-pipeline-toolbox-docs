.. _layout-animation:

Layout and Animation Tools
==========================

.. figure:: ../images/phase-movement.png

   The Layout and Animation Phase

.. toctree::
   :maxdepth: 1

   ../tools/createlayout
   ../tools/multishotexport
   ../tools/moctor
