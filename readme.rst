Nebula Pipeline Toolbox Documentation Read Me
==============================================

This repository uses `sphinx <https://www.sphinx-doc.org>`_ technology to create
documentation.


Building
--------

1. Clone this repository in your folder::

   $ git clone <repo_address> <folder>

#. `Install <https://www.sphinx-doc.org/en/master/usage/installation.html>`_
   sphinx into your environment::

   $ pip install -U sphinx

#. Using a terminal change to the directory of the repository::

   $ cd <path/to/folder>

#. Install sphinxcontrib-youtube plugin from the vendor directory::

   $ git submodule update --init
   $ pushd vendors/sphinxcontrib-youtube
   $ python setup.py install
   $ popd

#. To generate documentation do either of the following
    a. To generate html do the following::

       $ make html

    b. To generate pdf do the following [#f1]_::

       $ make latexpdf

    c. To generate a single html file do the following::

       $ make singlehtml

#. Find the documentation in the `_build` folder.


Dev Monitoring
---------------

In order to monitor your output as html using `gulp` and `browser-sync` do as
follows:

1. Make sure you have `nodejs` and `npm` installed along with `gulp-cli`.
#. Move to the folder containing this repo and run ``setup-dev.sh`` to install
   `gulp` and `browser-sync`.
#. In order to start monitoring run ``start-monitor.sh``
#. In order to stop monitoring run  ``stop-monitor.sh``


Contact
-------

Any queries can be forwarded to `Talha Ahmed <talha.ahmed@gmail.com>`


.. [#f1] Install the latex toolchain for latexpdf builder
