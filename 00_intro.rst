Introduction
============


This document attempts to give an overview of the Nebula Pipeline and some of
the functionality it offers.

In order to get an overview of the toolbox you can also watch the following
youtube video

.. only:: html

   .. youtube:: cB7mKCFLFcM
      :width: 100%


.. only:: latex

   .. figure:: images/intro.png
       :target: https://www.youtube.com/watch?v=cB7mKCFLFcM
       :width: 100%

.. centered:: |intro|

.. |intro| replace:: Nebula Pipeline Introduction


What is Nebula Pipeline?
------------------------

Nebula Pipeline is a collection of tools that deploy at different stages of
the pipeline in order to effectively manage a CG production by the use of a
central database.

The pipeline has evolved from automation attempts done in production.
Therefore, The advantage that these tools offers is more due to the fact that
it has been used to produced many hours of animated content across artists of
various roles and skillset.

Due to the nature of the projects it has been developed to support it is most
well-geared towards supporting episodic animations while it has also been used
effectively in creation of shorts and VFX features.


How to Read
-----------
This document briefly describe the various tools that make up the Nebula
Pipeline.

In order to put all the tools in context the :ref:`Background` chapter gives
an overall view of the pipeline that the scripts can be used together to
implement.

The next chapter, :ref:`The Tool Box`, goes on to describe each tool briefly.
Each description is aided with screen shots of the UI alongwith a listing of
its feature and small description of its usage along with the required
software. Each tool description is placed within the context of its use in the
production process so a fair idea can be developed of the value it offers.

The chapter :ref:`History` is more like a behind the scenes view of how the
pipeline came into being. It could make for interesting knowledge of those who
want to have a view of the rather small Pakistan CGI production industry.


Purpose and Audience
--------------------

.. container:: sidebar

   .. epigraph::
       Happy Reading!

       -- The Author

This document is intended to give an introductory insight to someone who would
like to employ the pipeline tools at their studio. However, the brief usage
descriptions can also be used by its users to get an idea about the normal
operation of each tool.
