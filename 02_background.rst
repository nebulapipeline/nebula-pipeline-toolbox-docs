Background
==========

Each tool contributes to a specific stage in the pipeline and some of them are
dependant upon the design assumptions and output of the other.  Therefore, it
is necessary to have an overview of the production pipeline and the context
for their development.

This is intended to give an insight to the reader of how the various tools in
the pipeline depend on each other and can function together to deliver a
production.

No Panacea!
-----------

There can be many ways of solving a production into a pipeline, while no one
solution is good enough for everyone. The effectiveness of the structure of a
pipeline depends upon the business model and culture of the organization, its
size and the history of the type of projects done under it.

With these factors in mind we shall now have a look at the proposed pipeline.


The Production Pipeline
-----------------------

The underlying pipeline can be schematically represented by the following diagram.

.. _figure-pipeline:
.. figure:: images/pipeline.png
   :target: prezi_

   Schematic View of the production pipeline

   Each phase of this image is represented by a `circle`. The `blue` boxes
   represent the artifacts produced in the phase and the `grey` boxes represent
   the processes involved to produce them.

   +----------+------------------+
   | Symbol   | Meaning          |
   +----------+------------------+
   | |phase|  | Pipeline Phase   |
   +----------+------------------+
   | |process|| Pipeline Process |
   +----------+------------------+
   | |artif|  | Artifacts        |
   +----------+------------------+

.. |phase| graphviz:: graphs/phase.gv

.. |process| graphviz:: graphs/process.gv

.. |artif| graphviz:: graphs/artifact.gv

.. _prezi: https://prezi.com/embed/xvucso-vmk6g/?bgcolor=ffffff&amp;lock_to_path=0&amp;autoplay=0&amp;autohide_ctrls=0&amp;landing_data=bHVZZmNaNDBIWnNjdEVENDRhZDFNZGNIUE43MHdLNWpsdFJLb2ZHanI0VTRKblRleUsvU1UvSjlxOFRSTkY0R2JnPT0&amp;landing_sign=vDXL1B0itul7Ey0BnnTKTPpxrq45EujVjP_dbB0B9Es


Observations
~~~~~~~~~~~~

Now we shall have a look at some important observations about the pipeline
view that this figure represents.

Business Model
``````````````

The image shows the division of the complete production process into three
main areas.  Pre-production, production and post-production. Moreover, the
assets, and production planning have been placed under the `production` area.
This is due to the nature of the projects done at many studios who integrate
into the industry as a production service company. This means that the
pre-production is done separately either outside the studio or in an art
department which employs the services of the production department to get the
project executed.

Considering asset development process as a part of production is very useful
in this business model. Even in the case when asset development is carried out
upstream, there is always some sort of prepping to be done for production.
Securing your assets is a good exercise of this business model and helps the
standardize the transition into production.


Medium Size Studio
``````````````````
The production pipeline that the current batch of scripts supports have been
developed for a medium-sized studio.

This meant that teams were organized into departments according to their
function in the production process. Each department has a supervisor who is
responsible for seeing through their portion of the production with the
limited hands at his disposal. One departments output is the input of the
other and standardization is needed. This also presents a requirement for
departmental review Such a studio is required to develop a pipeline which can
be used over several projects of often similar nature.

The second observation of note is that the assets processes are placed separately
from the `movement` and `imaging` section of the production. This actively
facilitates an episodic production where the assets may be finalized once and
may be used repetitively inside episodes after relatively.


Type of Projects
````````````````

.. |chk| unicode:: U+02713 .. CHECK

.. csv-table:: Types of Projects
   :header: Type, Animation, Live Action, VFX
   :widths: auto
   :align: center

   Short   , |chk| ,  , |chk| , ,
   Episode , |chk| ,  ,
   Feature ,       ,  , |chk|


Animation vs VFX and Live Action
'''''''''''''''''''''''''''''''''

VFX projects are different from animation projects because of their
requirement for photorealism. While photorealism brings a certain standard for
quality into play it also makes production complex because it is still
difficult to achieve.

Production wise it is different because of the dependance on `plates` i.e. the
footage from shooting of live actors or environments. This shifts the focus
towards post production as the goal is convincing integration of the plates
with the VFX. This means that much of the work is dictated by the look of the
final blended frame and many assets may just be 2-Dimensional in nature. This
entails a need for fluidity towards the end of production in the hands of the
`compositor` while any rigidity introduced by 3-Dimensionally synthesized
assets should primarilly cater for that need.


Episodic vs Feature and Shorts
'''''''''''''''''''''''''''''''

Although animation productions whether Episodic or Feature length will have
the same type of skill required from its artists but their production cycles
especially related to asset development will differ.

Episodic projects will have assets created once per many episodes
or production cycles. Most assets are produced without the knowledge of the
role in the script and many times they are used again without many changes
across episodes. Moreover, many versions of the same asset may be required in
different productions in parallel. While this scenario might be a source for
complications, it also provides opportunities for more reuse and thus
increased efficiency in utilization of the asset team.

Assets in Shorts and Features on the other hand maybe tinkered with deep into
production. Many assets, especially sets and environments, may only be used
in once or sequence and thus may constantly require improvements even after
being brought into productions, blurring the boundaries between assets
development and production processes. This is the reason why we may not find a
lot of emphasis on this distinction with shops who are focused on features.

Nebula Pipeline gives importance to the boundaries between asset development
and production as it provides an opportunity to formalize handing over of
tasks downstream. It also recognizes the requirement for supervisor review and
approvals these points.

Timely reviews and approvals will limit redoes to a small stage in pipeline
and result in a more efficient production cycle even in a feature scenario.
This might mean that an asset becomes less modifiable down stream in
production, and thus dictates the need to finalize them before being used in
production.


Environmental Effects vs Character Effects
``````````````````````````````````````````

Effects that introduce real complexity into a production are the 3D effects
that involve physical simulation. To deliver these convincingly requires
focused expertise and capable hardware, this is the reason they are expensive.

The software that are required to do these tasks integrate into the pipeline
through use of common formats such as `fbx` and `alembic`, and thus make the
task trivial on the outset. Moreover, the small number of jobs seen alongwith
the involvement of the major amount of effort to execute the task itself, it
is difficult to lockdown a common strategy of dealing with it.

Character Effects such as cloth and hair simulation on the other hand are
recurrent and more fundamental to the asset development process and the
production pipeline. The pipeline needs to adapt to the requirements of such
effects across all asset and production phases. The tools currently leave
space for character effects by allowing multiple geometry sets and
standardizing exists only for hand-animated geometry. However, there is room
for automation here if such protocols can be agreed upon.


Deployment Architecture
~~~~~~~~~~~~~~~~~~~~~~~

The pipeline deployment comes between the `users` and the 
`Network Attached Storage`. The degree of completeness of the pipeline is
inversely related to the number of times users need to manually access the
data in the `Network Attached Storage`

.. graphviz:: graphs/deployment.gv
   :align: center
   :caption: Deployment Architecture
   :name: Deployment

The Nebula Pipeline makes use of the :ref:`Tactic CMS <software-tactic>` to
manage its database and to provide production tracking.

.. _required-software:

Required Software
~~~~~~~~~~~~~~~~~~
The pipeline tools require the following software to run

.. _software-windows:

* `Windows OS <https://www.microsoft.com/en-us/windows>`_

.. _software-maya:

* `Autodesk Maya <https://www.autodesk.com/products/maya/overview>`_

.. _software-redshift:

* `Redshift 3D <https://www.redshift3d.com/>`_

.. _software-nuke:

* `The Foundry Nuke <https://www.foundry.com/products/nuke>`_

.. _software-heiro:

* `The Foundry Hiero <https://www.foundry.com/products/hiero>`_

.. _software-deadline:

* `Thinkbox Deadline <https://deadline.thinkboxsoftware.com/>`_

.. _software-tactic:

* `Southpaw Tactic Open Source <https://southpawtech.com/tactic-open-source/>`_

.. _software-ffmpeg:

* `FFMPEG <http://www.ffmpeg.org>`_

.. _software-image-magick:

* `Image Magick <https://www.imagemagick.org>`_
